# ZSH without theme
export ZSH="$HOME/.oh-my-zsh"
source $ZSH/oh-my-zsh.sh

# load starship
eval "$(starship init zsh)"

# Completion
source <(kubectl completion zsh)
source <(helm completion zsh)
source <(flux completion zsh)
source <(kustomize completion zsh)
source <(trivy completion zsh)
source <(minikube completion zsh)

# kubectl 
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
export KUBECTL_EXTERNAL_DIFF="diff --color -N -u"

# ALIAS
wk() {
    if [ ! -f ~/work/$1 ] && [ ! -d ~/work/$1 ]; then
        echo "Creating new directory $1"
        mkdir -p ~/work/$1
    fi
    cd ~/work/$1
}
compdef '_files -W ~/work' wk

alias ip='ip -color=auto'
alias kx='kubectx'
alias kn='kubens'
alias kl='kubectl'
alias kz='kustomize'
alias gg='git log --oneline --graph --all --decorate'
# docker
alias dex='docker exec -it'
alias fo='docker run --rm -it -v ~/.kube/config:/config -e KUBECONFIG=/config -u root fluxcd/flux-cli:v2.1.2'
alias dbt='docker build -t test .'
alias drt='docker run --rm -it --name test test'